<?php include $_SERVER['DOCUMENT_ROOT'] . '/header.inc.php'; ?>

<title>Home Page</title>

</head>
<body>

<h1>Home Page</h1>

<?php if (isset($_SESSION['username'])): ?>
	<a href="/logout">Logout</a>
<?php else: ?>
	<a href="/login">Login</a>
<?php endif; ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/footer.inc.php'; ?>
