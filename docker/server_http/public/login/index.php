<?php include $_SERVER['DOCUMENT_ROOT'] . '/header.inc.php'; ?>

<title>Login</title>

</head>
<body>

<h1>Login:</h1>

<?php if (isset($_SESSION['errore'])): ?>
	<p><?= $_SESSION['errore'] ?></p>
<?php
	unset($_SESSION['errore']);
	endif;
?>

<?php if (isset($_SESSION['username'])): ?>
	<p>Hai già eseguito il login come '<?= $_SESSION['username'] ?>'</p>
	<p>Esegui prima il <a href="/logout?redirect=/login">logout</a></p>
<?php else: ?>
	<form action="/login/login.php<?= isset($_GET['redirect']) ? '?redirect=' . $_GET['redirect'] : '' ?>" method="post">
		<input type="text" name="username" placeholder="Username..."><br>
		<input type="password" name="password" placeholder="Password..."><br>
		<button type="submit">Login</button>
	</form>
<?php endif; ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/footer.inc.php'; ?>
