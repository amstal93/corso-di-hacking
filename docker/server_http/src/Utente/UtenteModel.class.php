<?php

namespace Utente;

use PDOException;
use DbHandler;

class UtenteModel extends DbHandler {
	/**
	 * Cerca uno username nel database e ricava le sue informazioni
	 *
	 * @param  string      $username Lo username da cercare
	 * @return string|bool
	 */
	final protected function ottieniUtente(string $username) {
		// Crea la query SQL
		$sql = 'SELECT nome, cognome, password, ruolo FROM utenti WHERE username = ?';

		// Inizializza la prepared statement
		$stmt = $this->connessione()->prepare($sql);

		// Esegui la query SQL controllando che non ci siano errori
		try {
			$stmt->execute([$username]);
		} catch (PDOException $errore) {
			echo 'Errore SQL<br><br>Metodo: ' . __METHOD__ . '()';
			exit();
		}

		// Ritorna l'utente
		return $stmt->fetch();
	}
}
