<?php

spl_autoload_register('myAutoLoader');

/**
 * Cerca nella cartella '/src' il file '.class.php' contenente la classe e lo
 * richiede
 *
 * @param  string    $nomeClasse Il nome della classe da richiedere
 * @return null|bool
 */
function myAutoLoader(string $nomeClasse) {
	// Imposta le variabili
	$cartella = $_SERVER['DOCUMENT_ROOT'] . '/../src/';
	$estensione = '.class.php';

	// Componi il percorso
	$percorso = $cartella . str_replace('\\', '/', $nomeClasse) . $estensione;

	// Se il file contenente la classe non esiste, ritorna 'false'
	if (!file_exists($percorso)) {
		return false;
	}

	// Richiedi il file contenente la classe
	require_once $percorso;
}
