#!/bin/sh

IP_FTP='192.168.123.2'
USERNAME_FTP='azienda'
PASSWORD_FTP='password'

# Ciclo while infinito
while :; do
	# Entra in FTP nel server ed elenca i file
	lftp -u $USERNAME_FTP,$PASSWORD_FTP $IP_FTP <<EOF
		ls
		exit
EOF

	# Riposa per 10 secondi
	sleep 10
done
